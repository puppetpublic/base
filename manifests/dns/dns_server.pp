# This class is for domain name servers.
class base::dns::dns_server inherits base::dns {
  Base::Dns::Resolv_conf[$::fqdn_lc] { is_dns_server  => true  }

}
