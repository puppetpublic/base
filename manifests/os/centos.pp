#
# Rules specific to CentOS systems. Very thin class because with
# minor edits to redhat.pp, little additional config is needed 
class base::os::centos {

    # redhat.pp can handle most of the heavy lifting for CentOS
    include base::os::redhat

    # Preserve the native CentOS repos. These get squashed otherwise
    # tested on CentOS6 x86_64,
    $centos_repos = [ '/etc/yum.repos.d/CentOS-Base.repo',
                      '/etc/yum.repos.d/CentOS-Debuginfo.repo',
                      '/etc/yum.repos.d/CentOS-fasttrack.repo',
                      '/etc/yum.repos.d/CentOS-Media.repo',
                      '/etc/yum.repos.d/CentOS-Vault.repo' ]
    file { $centos_repos: ensure => present }

    # some important packages to have
    $centos_pkgs = [ 'dmidecode', 'kmod-openafs' ]
    package { $centos_pkgs: ensure => installed }

    # a couple of random packages we should have
    $centos_opt_pkgs = [ 'vim-enhanced', 'wget' ]
    package { $centos_opt_pkgs: ensure => installed }

    # group 37 (operator/rpm) does not exist on CentOS
    group { 'rpm':
      ensure => present,
      gid    => '37',
    }

    # CentOS repo rpm gpg key
    base::rpm::import { 'centos-rpmkey':
      url       => "/etc/pki/rpm-gpg/RPM-GPG-KEY-CentOS-${::lsbmajdistrelease}",
      signature => $::lsbmajdistrelease ? {
        '5' => 'gpg-pubkey-e8562897-459f07a4',
        '6' => 'gpg-pubkey-c105b9de-4e0fd3a3',
        '7' => 'gpg-pubkey-f4a80eb5-53a7ff4b',
      };
    }

}
