# modules/postfix/policyd.pp -- postfix-policyd engine model
#
# homepage: http://www.policyd.org/

class base::postfix::policyd {
    preseed_debconf { 'preseed-postfix-policyd':
        source => 'puppet:///modules/base/postfix/etc/apt/preseed-postfix-policyd',
        answer => 'postfix-policyd.*boolean.*false',
    }

    package { 'postfix-policyd':
        ensure  => installed,
        require => Preseed_debconf['preseed-postfix-policyd'],
    }

    exec { 'update-rc.d -f postfix-policyd remove':
        command     => '/usr/sbin/update-rc.d -f postfix-policyd remove',
        subscribe   => Package['postfix-policyd'],
        refreshonly => true,
    }

    file {
        '/etc/postfix-policyd.conf':
            ensure  => absent,
            require => Package['postfix-policyd'];
        '/etc/postfix-policyd':
            ensure  => directory,
            require => Package['postfix-policyd'];
        '/etc/postfix-policyd/letter':
            require => File['/etc/postfix-policyd'],
            source  => 'puppet:///modules/base/postfix/etc/postfix-policyd/letter';
    }
}

define base::postfix::policyd::config(
    $ensure = 'present'
) {
    $policy_name = $name

    $dbconfig = '/etc/postfix-policyd/policyd-db'
    $template = "/etc/postfix-policyd/$name.conf.tmpl"
    $config   = "/etc/postfix-policyd/$name.conf"
    $genconf  = "/usr/bin/generate-conf -c $dbconfig -t $template -n $config"

    exec { "restart policyd-$name":
        command     => "/usr/sbin/invoke-rc.d policyd-$name restart",
        refreshonly => true,
    }

    exec { "update-rc.d policyd-$name defaults":
        command     => "/usr/sbin/update-rc.d policyd-$name defaults",
        refreshonly => true,
    }

    exec { "generate policyd-$name config":
        command     => $genconf,
        notify      => Exec["restart policyd-$name"],
        subscribe   => File[$template],
        refreshonly => true,
    }

    case $ensure {
        present: {
            file {
                "/etc/init.d/policyd-$name":
                    content => template('base/postfix/policyd.init.erb'),
                    notify  => Exec["update-rc.d policyd-$name defaults"],
                    mode    => 755;
                $template:
                    source  => "puppet:///modules/base/postfix/$template",
                    require => [ File["/etc/init.d/policyd-$name"],
                                 File['/etc/postfix-policyd'],
                                 File[$dbconfig] ],
                    notify  => Exec["generate policyd-$name config"];
            }

            service { "policyd-$name":
                ensure     => running,
                hasrestart => true,
                enable     => true,
                require    => [ File["/etc/init.d/policyd-$name"],
                                File[$template] ];
            }
        }
        absent: {
        }
    }
}