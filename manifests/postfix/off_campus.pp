# Postfix class for systems that are off campus network.
#   outgoing email server only to stanford.edu addresses

class base::postfix::off_campus inherits base::postfix {
    Base::Postfix::Map['/etc/postfix/senders'] {
        content => "root@stanford.edu root@${::fqdn_lc}\n",
    }
    File['/etc/postfix/main.cf'] {
        source => 'puppet:///modules/base/postfix/etc/postfix/main.cf.off-campus',
    }
}
