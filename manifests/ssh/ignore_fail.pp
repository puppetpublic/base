# Ignore (via filter-syslog rules) failed ssh login attempts.  We don't want
# this on every server since we catch compromised hosts without it, but it can
# be used on any system where we have clients who keep mistyping their
# passwords.

class base::ssh::ignore_fail inherits base::ssh {
  file { '/etc/filter-syslog/ssh-ignore-fail':
    source => 'puppet:///modules/base/ssh/etc/filter-syslog/ssh-ignore-fail',
  }
}