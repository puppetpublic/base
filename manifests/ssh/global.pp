# Allow ssh from any IP address.  Any time we allow access from anywhere, we
# ignore failed login attempts, since there are tons of them.  Try to use
# base::ssh::defense instead of this class if at all possible.

class base::ssh::global inherits base::ssh::ignore_fail {
  Base::Iptables::Rule['ssh'] { source => '' }
}