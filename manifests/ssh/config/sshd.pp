# Create the sshd configuration.

# $listen_addresses: If you want to restrict the ssh service to listen only at
# certain addresses, specify with this parameter. Enter them as a
# comma-delimited list.
#
# Examples:
# listen_addresses => '192.168.1.1,192.168.2.1'
# listen_addresses => '192.168.1.1:22,192.168.2.1'
#
# See the sshd_config man page for what constitutes valid entries.
#
# If list_addresses is omitted, then the ListenAddress directive will be
# omitted from the sshd configuration file (which is equivalent to having
# sshd listen at _all_ addresses).
#
# $listen_ports: a comma-delimited list of ports to listen to. Defaults to
# "22". Example: "22,44".
#
# If you want to allow root to log in with a password, set
# rootloginwithpswd 'yes'. Otherwise, root logins with a password
# are not allowed.
#
# If you want to require Duo on login, set pam_duo to true (defaults to
# false).
#
# If $rootloginwithpswd is set to 'no' then we allow root logins using
# GSSAPI only.
#
# $max_sessions: if you want to allow more than the default of 10
# connections per network connection, set it here. This is particularly
# useful for bastion hosts.
# Default: undef
#
# $rootlogin_with_keypair: Allow the us of a public/private key-pair when
# logging in as "root".
# Default: false
#
# $extra_skip_duo_users: Due to problems with Duo, we skip Duo for users
# matching these strings: root,root.*,*.root,admin.*,*.admin. These users
# can ONLY use GSSAPI (no passwords) or, if $rootlogin_with_keypair is set
# to true, with a public/private key-pair. If you want to skip Duo for
# accounts IN ADDITION to this list, set this parameter to an array of
# such accounts. For example, if you want to skip Duo authentication for
#
#         root
#         root.*
#         *.root
#         admin.*
#         *.admin
#         wallet
#
# you would set $extra_skip_duo_users to ['wallet']
# Default: []

define base::ssh::config::sshd(
  $ensure            = 'present',
  $gitolite          = false,
  $ed25519           = false,
  $hostbased         = false,
  $pubkey            = false,
  $password          = true,
  $source            = undef,
  $template_path     = undef,
  $max_tries         = 5,
  $listen_addresses  = 'all',
  $listen_ports      = '22',
  $rootloginwithpswd = 'no',
  $rootlogin_with_keypair = false,
  $pam_duo           = false,
  $max_sessions      = 'NOT DEFINED',
  $extra_skip_duo_users = [],
) {
  if ($source) {
    $template = undef
  } else {
    if ($template_path) {
      $template = template($template_path)
    } else {
      $template = template('base/ssh/sshd_config.erb')
    }
  }
  file { $name:
    ensure  => $ensure,
    source  => $source,
    content => $template,
    notify  => Service['ssh'],
  }

  # If we are allowing ssh key-par logins, ignore the public key
  # authentications when filtering syslog.
  if ($pubkey) {
    file { '/etc/filter-syslog/ssh-pubkey':
      source => 'puppet:///modules/base/ssh/etc/filter-syslog/ssh-pubkey',
    }
  }

}
