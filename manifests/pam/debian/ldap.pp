# FIXME: move libpam-foreground and config (in pam.d/global/common-session)
# to the timeshare class, or something similar

class base::pam::debian::ldap inherits base::pam::debian {
  package {
    'libpam-ldap':             ensure => 'present';
    'libnss-ldap':             ensure => 'present';
    'libpam-openafs-kaserver': ensure => 'absent';
  }

  # A lot of this stuff is taken from s_timeshare, which is where it was
  # originally implemented.
  file {
    '/etc/ldap.conf':
      source  => 'puppet:///modules/base/pam/etc/ldap.conf';
    '/etc/libnss-ldap.conf':
      source  => 'puppet:///modules/base/pam/etc/libnss-ldap.conf';
    '/etc/nsswitch.conf':
      source  => 'puppet:///modules/base/pam/etc/nsswitch.conf';
    '/etc/pam.d/common-password':
      source  => 'puppet:///modules/base/pam/etc/pam.d/global/common-password',
      require => [ Package['libpam-krb5'] ];
    '/etc/pam_ldap.conf':
      source  => 'puppet:///modules/base/pam/etc/pam_ldap.conf';
  }

  File['/etc/pam.d/common-account'] {
    source => 'puppet:///modules/base/pam/etc/pam.d/global/common-account'
  }

  File['/etc/pam.d/common-auth'] {
    source => 'puppet:///modules/base/pam/etc/pam.d/global/common-auth'
  }

  File['/etc/pam.d/common-session'] {
    source => 'puppet:///modules/base/pam/etc/pam.d/global/common-session'
  }

}
