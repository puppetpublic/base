# Also allows remctl connections from Duke systems.

class base::remctl::duke inherits base::remctl {
  Base::Iptables::Rule['remctl'] { source +> [ '152.3.104.0/24' ] }
}