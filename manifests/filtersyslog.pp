# Install filter-syslog, which we use for auditing system logs, and its
# basic configuration.

class base::filtersyslog {

  # Install the filter-syslog package.
  package { 'filter-syslog':
    ensure => present
  }

  # Install the default filter-syslog configuration.
  file {
    '/etc/filter-syslog.conf':
      source  => 'puppet:///modules/base/filtersyslog/etc/filter-syslog.conf';
    '/etc/filter-syslog':
      ensure  => directory,
      recurse => true,
      purge   => true,
  }


}
