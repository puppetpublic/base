#
# Default up2date configuration for UNIX Systems machines.

define base::up2date::sources($jpackage17 = false, $useRHN = false, $remi = false,
                        $java5 = false, $java6 = false, $epel = false,
                        $shib = false, $ensure)
{
    case $ensure {
        present: {
            file { '/etc/sysconfig/rhn/sources':
                content => template('base/up2date/sources.erb'),
            }
        }
        absent: {
            file { '/etc/sysconfig/rhn/sources': ensure => absent }
        }
        default: { crit "Invalid ensure value: $ensure" }
    }
}

class base::up2date {
    case $::lsbdistrelease {
        4: {
            file {
                '/etc/sysconfig/rhn/up2date':
                    source => 'puppet:///modules/base/up2date/etc/sysconfig/rhn/up2date';
                '/etc/newsyslog.daily/up2date':
                    source => 'puppet:///modules/base/up2date/etc/newsyslog.daily/up2date';
            }

            base::up2date::sources { $::fqdn_lc: ensure => present }

            service { 'rhnsd':
                ensure => stopped,
                enable => false,
            }

        }
    }
}
