#
# Install or remove an apt-key key.  All keys should be located in
# the apt-key module so that we have some central management of them.

define base::apt_key::key($ensure) {
  case $ensure {
    absent: {
      file { "/etc/keyrings/$name":
        ensure => absent,
        notify => Exec['remove-apt-key'];
      }
    }
    present: {
      file { "/etc/keyrings/$name":
        source => "puppet:///modules/base/apt_key/$name",
        notify => Exec['add-apt-key'],
      }
    }
    default: {
      crit "Invalid ensure value: $ensure"
    }
  }
}

