# Configures a package that cleans out temporary files periodically.
#
# Debian and Ubuntu use tmpreaper.  Red Hat uses tmpwatch.  Both do roughly
# the same thing: run daily and clean out old files in /tmp that aren't being
# used.
#
# Note that Debian only cleans /tmp, not /var/tmp.  Red Hat cleans both, but
# clears /var/tmp on a longer schedule.

class base::tmpclean {
  case $::osfamily {
    'Debian': {
      package { 'tmpreaper': ensure => present }
      file { '/etc/tmpreaper.conf':
        source => 'puppet:///modules/base/tmpclean/etc/tmpreaper.conf',
      }
    }
    'RedHat': {
      package { 'tmpwatch': ensure => present }
      file { '/etc/cron.daily/tmpwatch':
        source => 'puppet:///modules/base/tmpclean/etc/cron.daily/tmpwatch',
      }
    }
  }
}
