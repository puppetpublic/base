# install and configure the puppet client
class base::puppetclient {
  file { '/etc/puppet':
    ensure => directory,
  }

  base::puppetclient::config { '/etc/puppet/puppet.conf':
    ensure => present,
  }

  # pin puppet and facter versions on RHELish systems
  # However, OracleLinux (6, at least) does not have yum-versionlock available
  if (($::osfamily == 'RedHat') and ($::operatingsystem != 'OracleLinux')) {
    include base::yumtools::yum_puppet_lock
    package { 'puppet':
      ensure  => present,
      require => [ Base::Puppetclient::Config['/etc/puppet/puppet.conf'],
                  Class['base::yumtools::yum_puppet_lock'] ],
    }
  } else { # Debianish systems and Oracle Linux
      package { 'puppet':
        ensure  => present,
        require => Base::Puppetclient::Config['/etc/puppet/puppet.conf'],
      }
  }

  # It appears that on recently updated wheezy servers, 2.x clients need
  # the ruby-json package Puppet systems.
  if ($::lsbdistcodename == 'wheezy') {
    package { 'ruby-json':
      ensure  => present,
    }
  }

  # On squeeze systems, default to the backports version of Puppet.  On wheezy
  # systems, default to the backports version of facter for proper detection
  # of Xen systems.
  if $::lsbdistcodename == 'squeeze' {
    file { '/etc/apt/preferences.d/puppet':
      source => 'puppet:///modules/base/puppetclient/etc/apt/preferences.d/puppet',
    }
  }
  if $::lsbdistcodename == 'wheezy' {
    file { '/etc/apt/preferences.d/facter':
      source => 'puppet:///modules/base/puppetclient/etc/apt/preferences.d/facter',
    }
  }

  # Install an auth.conf as additional protection against a Puppet
  # vulnerability that could allow unauthenticated Puppet nodes to
  # manipulate its configuration.
  file { '/etc/puppet/auth.conf':
    content => template('base/puppetclient/auth.conf.erb'),
  }

  # filter-syslog rules for the Puppet client.
  file { '/etc/filter-syslog/puppet':
    source => 'puppet:///modules/base/puppetclient/etc/filter-syslog/puppet';
  }

  # Check for a puppetd process on an hourly basis.
  file { '/etc/cron.hourly/check-puppet':
    source => 'puppet:///modules/base/puppetclient/etc/cron.hourly/check-puppet';
  }

  # Install a directory for external facts.  Used on both Debian and RHEL.
  file {
    '/etc/facter/':
      ensure => directory,
      mode   => '0644';
    '/etc/facter/facts.d':
      ensure => directory,
      mode   => '0644';
  }
}


