# Defines the DNS configuration of a system.

# You may set $no_resolv_conf to prevent Puppet from overriding the system's
# resolv.conf file.  This is helpful in DHCP environments.
# To enable, use Hiera to set base::dns::no_resolv_conf to "true".

# If you use a DNS cache, you can change base::dns::dns_cache in Hiera to
# true, and 127.0.0.1 will be set as the first nameserver.

# If the system's primary interface's IP address is in a known Livermore
# network, then the Livermore DNS will be added above the standard DNS servers.
# If dns_cache is also true, the Livermore DNS server will be second (right
# below 127.0.0.1).
class base::dns(
  $no_resolv_conf = false,
  $dns_cache      = 'NONE',
) {
  if (!$no_resolv_conf) {
    base::dns::resolv_conf { $::fqdn_lc:
      ensure    => present,
      dns_cache => $dns_cache,
    }
  }
}
