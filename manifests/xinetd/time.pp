# Set up the internal time services provided by xinetd.  This creates both the
# TCP and UDP versions of the daytime and time services.

class base::xinetd::time {
    include base::xinetd

    base::xinetd::config {
        'daytime-tcp':
            service     => 'daytime',
            server_type => 'INTERNAL',
            protocol    => 'tcp',
            description => 'POSIX ctime time service (TCP)';
        'daytime-udp':
            service     => 'daytime',
            server_type => 'INTERNAL UNLISTED',
            protocol    => 'udp',
            port        => 13,
            description => 'POSIX ctime time service (UDP)';
        'time-tcp':
            service     => 'time',
            server_type => 'INTERNAL',
            protocol    => 'tcp',
            description => 'RFC 868 time server for seconds since epoch (TCP)',
            cps         => '100 5';
        'time-udp':
            service     => 'time',
            server_type => 'INTERNAL UNLISTED',
            protocol    => 'udp',
            port        => 37,
            description => 'RFC 868 time server for seconds since epoch (TCP)',
            cps         => '100 5';
    }
}