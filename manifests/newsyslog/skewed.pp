# Install a variant of the daily newsyslog cron job that passes the -s 60
# argument to newsyslog.  This will treat timestamps for the purposes of file
# naming as if they were 60 minutes earlier than the current time, which can
# help with the stability of archive file names when there are long-running
# cron jobs that run before newsyslog.

class base::newsyslog::skewed inherits base::newsyslog {
  file { '/etc/cron.daily/newsyslog':
    source => 'puppet:///modules/base/newsyslog/etc/cron.daily/newsyslog';
  }
}
