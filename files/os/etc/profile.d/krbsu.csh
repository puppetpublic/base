# $Id: krbsu.csh 1426 2007-05-02 07:11:01Z digant $
#
# Setup the klogin = rlogin -x alias and run aklog

alias klogin "rlogin -x -f"
klist -s

if ( $? == 0 )then
    /usr/bin/aklog
endif
