# gsb-vatuqa iptables fragment
# $Id: public-timeshare 19110 2009-09-02 12:30:22Z moho $

#The public timeshares use egress filtering as part of intrusion-detection.

#log all  initiations of direct-to-smtp connections not owned by root
-A OUTPUT -p tcp --dport 25 --syn  -m owner  --gid-owner 37 -j LOG  --log-uid
#log all  initiations of direct-to-port ssmtp connections not owned by root
-A OUTPUT -p tcp --dport 465 --syn  -m owner  --gid-owner 37 -j LOG  --log-uid
#log all  initiations of direct-to-port "submission" connections not owned by root
-A OUTPUT -p tcp --dport 587 --syn  -m owner  --gid-owner 37 -j LOG  --log-uid
#Log any attempt to DoS or dictionary scan any remote ftp server
-A OUTPUT -p tcp --dport 21 --syn  -m limit --limit-burst 40 --limit 40/second -j ACCEPT
-A OUTPUT -p tcp --dport 21 --syn  -j LOG --log-uid
#Log any attempt to DoS or dictionary scan any remote ssh server
-A OUTPUT -p tcp --dport 22 --syn  -m limit --limit-burst 20 --limit 20/second -j ACCEPT
-A OUTPUT -p tcp --dport 22 --syn  -j LOG --log-uid
#Log any attempt to DoS or dictionary scan any telnet  server
-A OUTPUT -p tcp --dport 23 --syn  -m limit --limit-burst 20 --limit 20/second -j ACCEPT
-A OUTPUT -p tcp --dport 23 --syn  -j LOG --log-uid
#Log  and reject any attempt to DOS any DNS server (DROP not enabled yet)
-A OUTPUT -p udp --dport 53 -o lo -j ACCEPT
-A OUTPUT -p udp --dport 53  -m owner --gid-owner 37 -m limit --limit-burst 400 --limit 400/second -j ACCEPT
-A OUTPUT -p udp --dport 53  -m owner --gid-owner 37   -j LOG --log-uid
#-A OUTPUT -p udp --dport 53  -m owner --gid-owner 37   -j DROP
#Log any attempt to DoS or dictionary scan any DHCP server
-A OUTPUT -p udp -m multiport --dports 67,68 -m limit --limit-burst 20 --limit 20/second -j ACCEPT
-A OUTPUT -p udp -m multiport --dports 67,68  -j LOG --log-uid
#Log any attempt to DoS or dictionary scan any SNMP server
-A OUTPUT -p udp -m multiport --dports 161,162 -m owner --gid-owner 37 -m limit --limit-burst 50 --limit 50/second -j ACCEPT
-A OUTPUT -p udp -m multiport --dports 161,162  -m owner --gid-owner 37 -j LOG --log-uid
-A OUTPUT -p tcp -m multiport --dports 161,162 -m owner --gid-owner 37 --syn  -m limit --limit-burst 20 --limit 20/second -j ACCEPT
-A OUTPUT -p tcp -m multiport --dports 161,162 -m owner --gid-owner 37 --syn  -j LOG --log-uid
#Log any attempt to DoS or dictionary scan any SMB session server
-A OUTPUT -p tcp --dport 139 --syn  -m limit --limit-burst 20 --limit 20/second -j ACCEPT
-A OUTPUT -p tcp --dport 139 --syn  -j LOG --log-uid
#Log any attempt to DoS or dictionary scan any SOCKS server
-A OUTPUT -p tcp --dport 1080 --syn  -m limit --limit-burst 20 --limit 20/second -j ACCEPT
-A OUTPUT -p tcp --dport 1080 --syn  -j LOG --log-uid
-A OUTPUT -p udp --dport 1080  -m limit --limit-burst 2000 --limit 2000/second -j ACCEPT
-A OUTPUT -p udp --dport 1080  -j LOG --log-uid
#Log any attempt to DoS or dictionary scan any SQL server
-A OUTPUT -p tcp -m multiport --dports 1433,3306 --syn  -m limit --limit-burst 20 --limit 20/second -j ACCEPT 
-A OUTPUT -p tcp -m multiport --dports 1433,3306 --syn  -j LOG --log-uid
#Log any attempt to DoS or dictionary scan any RDP server
-A OUTPUT -p tcp --dport 3389 --syn  -m limit --limit-burst 20 --limit 20/second -j ACCEPT
-A OUTPUT -p tcp --dport 3389 --syn  -j LOG --log-uid
#Log any attempt to DoS or dictionary scan any SIP server
-A OUTPUT -p tcp -m multiport --dports 5050,5060,5061 --syn  -m limit --limit-burst 20 --limit 20/second -j ACCEPT
-A OUTPUT -p tcp -m multiport --dports 5050,5060,5061 --syn  -j LOG --log-uid
-A OUTPUT -p udp -m multiport --dports 5050,5060,5061  -m limit --limit-burst 400 --limit 400/second -j ACCEPT
-A OUTPUT -p udp -m multiport --dports 5050,5060,5061  -j LOG --log-uid
#Log any attempt to DoS or dictionary scan any IRC server
-A OUTPUT -p tcp -m multiport --dports 6660:6669,7000 --syn  -m limit --limit-burst 20 --limit 20/second -j ACCEPT
-A OUTPUT -p tcp -m multiport --dports 6660:6669,7000 --syn  -j LOG --log-uid
-A OUTPUT -p udp -m multiport --dports 6660:6669  -m limit --limit-burst 400 --limit 400/second -j ACCEPT
-A OUTPUT -p udp -m multiport --dports 6660:6669  -j LOG --log-uid
-A OUTPUT -p udp -m multiport --dports 7000 -m owner --gid-owner 37 -m limit --limit-burst 400 --limit 400/second -j ACCEPT
-A OUTPUT -p udp -m multiport --dports 7000  -m owner --gid-owner 37 -j LOG --log-uid

#Disallow any connections to the X-protocol ports
-A INPUT -p tcp --dport 5800:6100 --syn -j REJECT --reject-with tcp-reset
-A INPUT -p udp --dport 5800:6100  -j REJECT --reject-with icmp-port-unreachable

# Let any campus hosts connect to most high-numbered ports
-A INPUT -p tcp -s 171.64.0.0/14   --sport 1024: --dport 1024: --syn -j ACCEPT
-A INPUT -p udp -s 171.64.0.0/14   --sport 1024: --dport 1024: -j ACCEPT
-A INPUT -p tcp -s 172.24.0.0/14   --sport 1024: --dport 1024: --syn -j ACCEPT
-A INPUT -p udp -s 172.24.0.0/14   --sport 1024: --dport 1024: -j ACCEPT
-A INPUT -p tcp -s 68.65.160.0/20  --sport 1024: --dport 1024: --syn -j ACCEPT
-A INPUT -p udp -s 68.65.160.0/20  --sport 1024: --dport 1024: -j ACCEPT
-A INPUT -p tcp -s 128.12.0.0/16   --sport 1024: --dport 1024: --syn -j ACCEPT
-A INPUT -p udp -s 128.12.0.0/16   --sport 1024: --dport 1024: -j ACCEPT
-A INPUT -p tcp -s 204.63.224.0/21 --sport 1024: --dport 1024: --syn -j ACCEPT
-A INPUT -p udp -s 204.63.224.0/21 --sport 1024: --dport 1024: -j ACCEPT
#Let the world connect to ports used by mosh-server
-A INPUT -p udp --sport 1024: --dport 60000:61000 -j ACCEPT
